GOOGLEAPIS_DIR=dep/googleapis
PROTO_FILE=complaints.proto

.PHONY: dep gen run get-grpc get-rest

dep:
	rm -rf dep
	mkdir dep
	git clone https://github.com/googleapis/googleapis ${GOOGLEAPIS_DIR}
	sudo apt install protobuf-compiler
	go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger@latest

gen:
	rm -rf gen
	mkdir gen
	protoc -I${GOOGLEAPIS_DIR} --include_imports --include_source_info --descriptor_set_out=gen/complaint.pb --experimental_allow_proto3_optional --proto_path $(shell dirname ${PROTO_FILE}) $(shell basename ${PROTO_FILE})
	protoc -I${GOOGLEAPIS_DIR} --swagger_out=logtostderr=true,allow_merge=true,merge_file_name=gen/complaint:. --experimental_allow_proto3_optional  --proto_path $(shell dirname ${PROTO_FILE}) $(shell basename ${PROTO_FILE})
	mv gen/complaint.swagger.json gen/complaint.json
	npx @redocly/cli build-docs gen/complaint.json -o gen/complaint.html

run:
	envoy -c config.yaml

get-grpc:
	grpcurl -v -d '"f3826f57-3cfa-40aa-8769-ab987f994b70"' -plaintext localhost:4004 gitlab.com.mailstep.mailship.mailship_20_monoreposervice_complaint.proto.ServiceComplaint/GetComplaint

get-rest:
	curl -v localhost:8000/complaint/f3826f57-3cfa-40aa-8769-ab987f994b70
