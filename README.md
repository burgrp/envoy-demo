# Envoy demo
How to do GRPC to REST transcoding by Envoy proxy.

## Prerequisites

Although `make dep` downloads some stuff needed for the demo, it is expected that you have `go`, `git`, `protoc` and `node.js` installed already.

The demo expects the Complaints GRPC service running on port 4004.

## Make targets

- `dep`: Sets up the necessary dependencies for the demo.
- `gen`: Generates protobuf and Swagger documentation.
- `run`: Runs the Envoy proxy with the provided configuration.
- `get-grpc`: Calls `complaint.Complaint` GRPC service.
- `get-rest`: Calls `GET complaint/{id}` REST service.

## Links

- [gRPC-JSON transcoder used in this demo](https://www.envoyproxy.io/docs/envoy/v1.29.0/configuration/http/http_filters/grpc_json_transcoder_filter#config-http-filters-grpc-json-transcoder)
- [...and all Envoy filters](https://www.envoyproxy.io/docs/envoy/v1.29.0/configuration/http/http_filters/http_filters)
- [Google doc on Transcoding](https://cloud.google.com/endpoints/docs/grpc/transcoding)
- [Mapping reference](https://cloud.google.com/service-infrastructure/docs/service-management/reference/rpc/google.api#http)
